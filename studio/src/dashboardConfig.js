export default {
  widgets: [
    { name: "structure-menu" },
    {
      name: "project-info",
      options: {
        __experimental_before: [
          {
            name: "netlify",
            options: {
              description:
                "NOTE: Because these sites are static builds, they need to be re-deployed to see the changes when documents are published.",
              sites: [
                {
                  buildHookId:
                    "60f84a82a6b4b5080c054587",
                  title: "Sanity Studio",
                  name: "sanity-gatsby-blog-hello-studio",
                  apiId: "bc50e43b-0841-4914-af8d-c8386ef2ef52",
                },
                {
                  buildHookId: "60f84a82ea62cc096256d879",
                  title: "Blog Website",
                  name: "sanity-gatsby-blog-hello",
                  apiId: "15a99ca7-bb25-4210-8446-fa7a8cfa1ae3",
                },
              ],
            },
          },
        ],
        data: [
          {
            title: "GitHub repo",
            value:
              "https://github.com/aufaraditya/sanity-gatsby-blog-hello",
            category: "Code",
          },
          {
            title: "Frontend",
            value: "https://sanity-gatsby-blog-hello.netlify.app",
            category: "apps",
          },
        ],
      },
    },
    { name: "project-users", layout: { height: "auto" } },
    {
      name: "document-list",
      options: {
        title: "Recent blog posts",
        order: "_createdAt desc",
        types: ["post"],
      },
      layout: { width: "medium" },
    },
  ],
};
