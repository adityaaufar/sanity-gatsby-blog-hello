module.exports = {
  sanity: {
    projectId:
      process.env.GATSBY_SANITY_PROJECT_ID || "ie27q6at",
    dataset: process.env.GATSBY_SANITY_DATASET || "production",
  },
};
